#!/usr/bin/env bash
today=`date +%Y-%m-%d--%H-%M-%S`
nohup scrapy crawl scielo_article_listing >> ~/logs/scielo/output_$today.log &
rm -f output.log
ln -s ~/logs/scielo/output_$today.log output.log
echo "$!" > scielo.pid
__author__ = 'Maksim_Kviatkouski'
from history.logic import StoreBase


class StoreNonErrors(StoreBase):
    def store_if(self, spider, request, response):
        return response.status < 400

__author__ = 'Maksim_Kviatkouski'
import os
import pickle

from scrapy.statscol import StatsCollector


class PersistentStatsCollector(StatsCollector):
    def __init__(self, crawler):
        super(PersistentStatsCollector, self).__init__(crawler)
        # need to load stats from jobdir here
        self.jobdir = crawler.settings.get('JOBDIR')
        if self.jobdir and os.path.exists(self.statefn):
            with open(self.statefn, 'rb') as f:
                self._stats = pickle.load(f)
        else:
            self._stats = {}

    def _persist_stats(self, stats, spider):
        if self.jobdir:
            with open(self.statefn, 'wb') as f:
                pickle.dump(stats, f, protocol=2)

    @property
    def statefn(self):
        return os.path.join(self.jobdir, 'crawler.state')

__author__ = 'Maksim_Kviatkouski'
from time import sleep

from boto.s3.connection import S3Connection
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from scrapy import log


class ResponseDumpMiddleware(object):
    def __init__(self, crawler):
        self.settings = crawler.settings
        dispatcher.connect(self.spider_opened, signal=signals.spider_opened)
        dispatcher.connect(self.spider_closed, signal=signals.spider_closed)

    def spider_opened(self, spider):
        self.conn = S3Connection(self.settings.get('AWS_ACCESS_KEY_ID'), self.settings.get('AWS_SECRET_ACCESS_KEY'))
        self.bucket = self.conn.get_bucket('scielo.xml.dump', validate=False)
        log.msg('Connected to %s bucket for dumping xml' % 'scielo.xml.dump')

    def spider_closed(self, spider):
        self.conn.close()
        log.msg('Closed connection to %s bucket' % 'scielo.xml.dump')

    def process_response(self, request, response, spider):
        if response.url.find('articleXML.php') != -1:
            key = request.meta.get('item', {}).get('id', '')
            if id:
                s3_key = self.bucket.new_key(key + ".xml")
                s3_key.set_contents_from_string(response.body)
                s3_key.close()
                log.msg('Dumped %s article into %s bucket' % (key, 'scielo.xml.dump'))
        return response

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)


class PausingMiddleware(object):
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)

    def __init__(self, crawler):
        self.crawler = crawler

    def process_response(self, request, response, spider):
        if response.status in [503, 500]:
            log.msg('Pausing engine for 5 minutes due to 503 at %s' % response.url, log.WARNING)
            self.crawler.engine.pause()
            sleep(60 * 5)
            self.crawler.engine.unpause()
        return response

# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ScieloItem(scrapy.Item):
    timestamp = scrapy.Field()
    spiderVersion = scrapy.Field()

    id = scrapy.Field()  #
    url = scrapy.Field()  #
    title = scrapy.Field()  #
    volume = scrapy.Field()  #
    year = scrapy.Field()  #
    number = scrapy.Field()  #
    pages = scrapy.Field()  # ?
    doi = scrapy.Field()  #
    article_title = scrapy.Field()  #
    author = scrapy.Field()  #
    # releaseInfo = scrapy.Field()
    keywords = scrapy.Field()  #
    ptKeywords = scrapy.Field()  #
    # fullTextHtmlLink = scrapy.Field()
    fullTextPdfLink = scrapy.Field()
    abstracts = scrapy.Field()  #
    references = scrapy.Field()  #
    copyright_statement = scrapy.Field()  #
    copyright_year = scrapy.Field()  #
    onlineISSN = scrapy.Field()
    printISSN = scrapy.Field()
    # article_type = scrapy.Field()
    # imageUrls = scrapy.Field()
    pass


class AuthorItem(scrapy.Item):
    given_name = scrapy.Field()  #
    surname = scrapy.Field()  #
    institution = scrapy.Field()  #
    address = scrapy.Field()  #


class ReferenceItem(scrapy.Item):
    label = scrapy.Field()  #
    authors = scrapy.Field()  #
    link = scrapy.Field()  # ?
    title = scrapy.Field()  #
    source = scrapy.Field()  #
    year = scrapy.Field()  #
    volume = scrapy.Field()  #
    page_range = scrapy.Field()  #


class ReferenceAuthorItem(scrapy.Item):
    given_name = scrapy.Field()  #
    surname = scrapy.Field()  #


class UrlItem(scrapy.Item):
    url = scrapy.Field()

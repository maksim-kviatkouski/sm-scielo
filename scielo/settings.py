# -*- coding: utf-8 -*-

# Scrapy settings for scielo project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'scielo'

SPIDER_MODULES = ['scielo.spiders']
NEWSPIDER_MODULE = 'scielo.spiders'

DOWNLOADER_MIDDLEWARES = {
    # 'scielo.randomproxy.RandomProxy': 100,
    # 'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'scielo.middleware.PausingMiddleware': 550,
    'scielo.middleware.ResponseDumpMiddleware': 875
    # 'history.middleware.HistoryMiddleware': 901
}

PROXY_LIST = 'scielo/proxies.txt'

EPOCH = True
HISTORY = {
    'STORE_IF': 'scielo.logic.StoreNonErrors',
    'RETRIEVE_IF': 'history.logic.RetrieveAlways',
    'BACKEND': 'history.storage.S3CacheStorage',
    'S3_ACCESS_KEY': 'AKIAJ5OEW7R44CI67TNQ',
    'S3_SECRET_KEY': '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',
    'S3_BUCKET': 'scielo.cache',
    'USE_PROXY': False,
}

FEED_URI = 's3://scraping.items/%(name)s/%(time)s.jsonlines'
FEED_FORMAT = 'jsonlines'
AWS_ACCESS_KEY_ID = 'AKIAJ5OEW7R44CI67TNQ'
AWS_SECRET_ACCESS_KEY = '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'

CONCURRENT_REQUESTS = 5
CONCURRENT_REQUESTS_PER_DOMAIN = 10

EXTENSIONS = {
    'scrapy.contrib.memusage.MemoryUsage': 500,
    # 'scrapy.webservice.WebService': 500,
    'scrapy.contrib.corestats.CoreStats': 500,
    'scrapy.contrib.debug.StackTraceDump': 500
    # 'scrapy.telnet.TelnetConsole': 500,
    # 'scrapy.webservice.WebService': 500
}

RETRY_TIMES = 20

# WEBSERVICE_ENABLED = True
# WEBSERVICE_PORT = [6080,7030]
MEMDEBUG_ENABLED = True
MEMUSAGE_WARNING = 1024
MEMUSAGE_LIMIT = 1536
MEMUSAGE_NOTIFY_MAIL = 'kvyatkovskij@gmail.com'
MEMUSAGE_REPORT = True

# AUTOTHROTTLE_ENABLED = True
# AUTOTHROTTLE_START_DELAY = 1.0
# AUTOTHROTTLE_MAX_DELAY = 60.0
# AUTOTHROTTLE_DEBUG = True

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'scielo (+http://www.yourdomain.com)'

STATS_CLASS = 'scielo.extensions.PersistentStatsCollector'
DUPEFILTER_CLASS = 'scrapy.dupefilter.BaseDupeFilter'

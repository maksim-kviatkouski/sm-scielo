__author__ = 'Maksim_Kviatkouski'

import json
import chardet
import scielo
from datetime import datetime
from scielo.items import ScieloItem, AuthorItem, ReferenceItem, ReferenceAuthorItem, UrlItem
from scrapy import log, Spider, Selector
from scrapy.contrib.spiders import XMLFeedSpider
from scrapy import Request
from lxml import etree
from urlparse import urlparse


# MAIN_URL = 'http://www.scielo.br/oai/scielo-oai.php?verb=ListIdentifiers&metadataPrefix=oai_dc_agris'
MAIN_URL = 'http://www.scielo.br/oai/scielo-oai.php?verb=ListIdentifiers&metadataPrefix=oai_dc_agris&resumptionToken=HR__S1519-69842004000400010::::oai_dc'
ARTICLE_XML_URL = 'http://www.scielo.br/scieloOrg/php/articleXML.php?pid=%s&lang=en'
ARTICLE_XML_URL_TEMPLATE = 'http://%s/scieloOrg/php/articleXML.php?pid=%s&lang=en'


class ScieloSpider(Spider):
    name = 'scielo'
    # allowed_domains = ['scielo.sld.cu', 'www.scielo.cl', 'www.scielo.org.mx', 'www.scielosp.org', 'www.scielo.org.co',
    #                    'www.scielo.org.pe', 'www.scielo.sa.cr', 'www.scielo.org.za', 'www.scielo.edu.uy',
    #                    'www.scielo.org.ar', 'scielo.isciii.es', 'socialsciences.scielo.org']
    allowed_domains = ['www.scielo.cl']

    def start_requests(self):
        with open('article_links.jl', 'r') as f:
            for line in f:
                url = json.loads(line)['url']
                yield Request(url, callback=self.parse_article)

    def parse_article(self, response):
        log.msg('Got article at %s' % response.url)
        content = response.body
        encoding = chardet.detect(content)['encoding']
        if (encoding != 'utf-8'):
            content = content.decode(encoding, 'replace').encode('utf-8')
        xml = etree.fromstring(content)
        item = ScieloItem()
        item['timestamp'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['spiderVersion'] = scielo.__version__
        item['article_title'] = xml.xpath('//journal-title/text()')
        item['url'] = response.url
        item['volume'] = xml.xpath('/article/front/article-meta/volume/text()')
        item['year'] = xml.xpath('/article/front/article-meta/pub-date[@pub-type="pub"]/year/text()')
        item['number'] = xml.xpath('/article/front/article-meta/numero/text()')
        item['pages'] = ''
        item['doi'] = xml.xpath('/article/front/article-meta/article-id[@pub-id-type="doi"]/text()')
        item['title'] = xml.xpath('/article/front/journal-meta/journal-title/text()')
        item['author'] = []
        for n in xml.xpath('/article/front/article-meta/contrib-group/contrib[@contrib-type="author"]'):
            a = AuthorItem()
            a['given_name'] = n.xpath('./name/given-names/text()')
            a['surname'] = n.xpath('./name/surname/text()')
            aff_id = n.xpath('./xref[@ref-type="aff"]/@rid')
            aff_id = aff_id[0] if aff_id else None
            if aff_id:
                aff_node = xml.xpath('/article/front/article-meta/aff[@id="%s"]' % aff_id)[0]
                a['institution'] = aff_node.xpath('./institution/text()')
                a['address'] = aff_node.xpath('./add-line/text()')
        item['keywords'] = xml.xpath('/article/front/article-meta/kwd-group/kwd[@lng="en"]/text()')
        item['ptKeywords'] = xml.xpath('/article/front/article-meta/kwd-group/kwd[@lng="pt"]/text()')
        item['abstracts'] = " ".join(xml.xpath('/article/front/article-meta/abstract[@xml:lang="en"]//*/text()'))
        item['references'] = []
        for n in xml.xpath('/article/back/ref-list/ref'):
            reference = ReferenceItem()
            reference['authors'] = []
            for ra in n.xpath('./person-group/name'):
                a = ReferenceAuthorItem()
                a['given_name'] = ra.xpath('./given-names/text()')
                a['surname'] = ra.xpath('./surname/text()')
                reference['authors'].append(a)
            reference['label'] = n.xpath('./label/text()')
            reference['title'] = n.xpath('./nlm-citation/article-title[@xml:lang="en"]/text()')
            reference['source'] = n.xpath('./nlm-citation/source/text()')
            reference['year'] = n.xpath('./nlm-citation/year/text()')
            reference['volume'] = n.xpath('./nlm-citation/volume/text()')
            reference['page_range'] = n.xpath('./nlm-citation/page-range/text()')
            item['references'].append(reference)
        item['copyright_year'] = xml.xpath('/article/front/article-meta/copyright-year/text()')
        item['copyright_statement'] = xml.xpath('/article/front/article-meta/copyright-statement/text()')
        item['printISSN'] = xml.xpath('/article/front/journal-meta/issn/text()')
        return item


class ScieloArticleListingSpider(Spider):
    name = 'scielo_article_listing'
    # allowed_domains = ['scielo.sld.cu', 'www.scielo.cl', 'www.scielo.org.mx', 'www.scielosp.org', 'www.scielo.org.co',
    #                    'www.scielo.org.pe', 'www.scielo.sa.cr', 'www.scielo.org.za', 'www.scielo.edu.uy',
    #                    'www.scielo.org.ar', 'scielo.isciii.es', 'socialsciences.scielo.org']
    #allowed_domains = ['www.scielo.cl'] # not scraped well, 503s after some time
    allowed_domains = ['www.scielo.cl']
    start_urls = ['http://%s/oai/scielo-oai.php?verb=ListIdentifiers&metadataPrefix=oai_dc' % domain for domain in
                  allowed_domains]


    def parse(self, response):
        current_domain = urlparse(response.url).netloc
        selector = Selector(response, type='xml')
        for _id in selector.xpath('//*[local-name() = "identifier"]/text()').extract():
            id = _id.replace('oai:scielo:', '')
            if not id:
                log.msg('Got no ID at %s' % response.url, log.WARNING)
            item = UrlItem()
            item['url'] = ARTICLE_XML_URL_TEMPLATE % (current_domain, id)
            yield item
        resumptionToken = selector.xpath('//*[local-name() = "resumptionToken"]/text()')
        resumptionToken = resumptionToken.extract()[0] if resumptionToken.extract() else None
        if resumptionToken:
            url = 'http://%s/oai/scielo-oai.php?verb=ListIdentifiers&metadataPrefix=oai_dc_agris&resumptionToken=%s' % (
                current_domain, resumptionToken)
            yield Request(url, priority=5)

            
class ScieloDomainListingSpider(Spider):
    name = 'scielo_domain_listing'
    start_urls = [
        'http://www.scielo.org/applications/scielo-org/php/secondLevel.php?xml='
        'secondLevelForAlphabeticList&xsl=secondLevelForAlphabeticList'
    ]
    API_URL_POSTFIX = '/oai/scielo-oai.php?verb=ListIdentifiers&metadataPrefix=oai_dc'

    def parse(self, response):
        links = response.xpath('//div[@class="content"]//@href').extract()
        domains = set()
        for l in links:
            o = urlparse(l)
            d = o.netloc
            if d not in domains:
                domains.add(d)
                api_url = o.scheme + "://" + d + self.API_URL_POSTFIX
                yield Request(api_url, callback=self.filter_domain)

    def filter_domain(self, response):
        if response.status == 200:
            item = UrlItem()
            item['url'] = urlparse(response.url).netloc
            return item

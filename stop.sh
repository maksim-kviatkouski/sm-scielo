#!/usr/bin/env bash
pid=`cat scielo.pid`
kill $pid
echo "Waiting for process to stop..."
while ps -p `cat scielo.pid` > /dev/null; do sleep 1; done
echo "Done"